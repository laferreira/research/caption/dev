#!/bin/bash

CMD="docker run --rm -it \
       --gpus all \
       -p 8888:8888 \
       --mount type=bind,source=$HOME/.jupyter,destination=/root/.jupyter \
       --mount type=bind,source=$PWD/src,destination=/notebooks \
       --mount type=bind,source=$HOME/files,destination=/notebooks/data \
       --workdir /notebooks \
       --name caption \
       caption"
$CMD $1
