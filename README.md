# CAPTION v2

This repo continues the development of the research from [CAPTION](http://www.google.com/url?q=http%3A%2F%2Fstaff.scem.uws.edu.au%2F~simeon%2FIMA2019%2FIMA2019_paper_5_cr.pdf&sa=D&sntz=1&usg=AFQjCNFyZQ8F8ZteOjIHZO097_9R2axf3g).

As  before, it provides the code needed to run the experiments along with an environment (a docker container). Files frlom FOIL-COCO, MS-COCO, TensorFlow model zoo and Sallient Object Detection are not provided but can be easily downloaded.
