class Prettifier:
    
    from matplotlib import pyplot as plt
    from matplotlib import patches
    from PIL import Image
    
# ---------------------------------------------------------------------------- #
    def plot_images(self, images, objs_info, planes_info):
        
        marked_img, image, sod_img = images
        
        fig, (ax1, ax2, ax3) = self.plt.subplots(1, 3, sharex=True, sharey=True,
                                                 gridspec_kw={'hspace': 0,
                                                              'wspace': 0},
                                                 figsize=(30, 15),)

        ax1.imshow(marked_img)
        ax2.imshow(image)
        ax3.imshow(sod_img, cmap='gray')

        for objs in planes_info.values():
            for obj in objs:
                y0, x0, y1, x1 = obj['box']
                w = x1 - x0
                h = y1 - y0
                r = self.patches.Rectangle((x0, y0), w, h,
                                           facecolor='none',
                                           linewidth=5,
                                           edgecolor='r')
                ax3.add_patch(r)

        self.plt.show()
    
# ---------------------------------------------------------------------------- #
    def print_info(self, tasks, foil_entry):
        
        t1, t2, t3 = tasks
        caption = foil_entry['caption']
        
        self.print_line()
        print(f"Image: {foil_entry['image_id']}")
        print(f"Caption: {caption}\n")

        print("Is caption a foil?")
        print(f"CAPTION: {t1}")
        print(f"FOIL:\t {foil_entry['foil']}\n")

        if t1 or foil_entry['foil']:
            print("Errors found (foil):")
            print(f"FOIL:\t {foil_entry['foil_word']}")
            for t, v in {t: v for t, v in t2.items() if v}.items():
                print(f"CAPTION: {v} ({t})")

            print("\nCorrection:")
            for f_type, corr in {k: v for k, v in t3.items() if v}.items():
                for k, v in corr.items():
                    print(f"CAPTION: {k} -> {v} ({f_type})")

            foil_word = foil_entry['foil_word']
            target_word = foil_entry['target_word']
            print(f"FOIL:\t {foil_word} -> {target_word}")
            
        if t1:
            print("\nSuggested captions:")
            for foil_corrections in t3.values():
                if foil_corrections:
                    for foil_word, corrections in foil_corrections.items():
                        for correction in corrections:
                            print(caption.replace(foil_word, correction))
                            

        self.print_line()

# ---------------------------------------------------------------------------- #
    def show_all_info(self, foil_entry, results, same_image):
        image, marked_img, sod_img = results['debug']['images']
        objs_info = results['debug']['objs_info'] 
        planes_info = results['debug']['planes_info'] \
                      if 'planes_info' in results['debug'] \
                      else dict()

        if not same_image:
            images = (marked_img, image, sod_img)
            self.plot_images(images, objs_info, planes_info)

        self.print_info(results['tasks'], foil_entry)

# ---------------------------------------------------------------------------- #
    def print_line(self, length=80, char="-"):
        print(char*length)
        
# ---------------------------------------------------------------------------- #
