class TaskSolver:
    
    from itertools import combinations
    
# ---------------------------------------------------------------------------- #
    def format_output(self, error_types, foil_list, correction_list):
        t1 = any(error_types)
        t2 = {k: v for k, v in zip(error_types, foil_list)}
        t3 = {k: v for k, v in zip(error_types, correction_list)}
        
        return t1, t2, t3
    
# ---------------------------------------------------------------------------- #
    def task1_solver(self, errors):
        return any(errors)
    
# ---------------------------------------------------------------------------- #
    def task2_solver(self, corrections):
        return list(corrections.keys())
    
# ---------------------------------------------------------------------------- #
    def task3_noun(self, caption, image):
        # compare caption and image
        # possible correction is a term with the same name:supercategory in both
        # caption and image
        corrections = {noun: [obj for obj, (name_o, sc_o) in image.items()
                              if name_n == name_o or sc_n == sc_o]
                       for noun, (name_n, sc_n) in caption.items()}

        # clean empty lists for keys
        # if there are no substitutions, it becomes an empty list
        corrections = {k: v for k, v in corrections.items() if v}

        return corrections
    
# ---------------------------------------------------------------------------- #
    def task3_plane(self, planes):
        corrections = {o[-1][0]: [o[-1] for o in l]
                       for o, l in planes.items() if l}

        return corrections
    
# ---------------------------------------------------------------------------- #