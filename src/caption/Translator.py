class Translator:
    from itertools import product
    from operator import itemgetter
    import numpy as np
    from scipy import stats
    
# ---------------------------------------------------------------------------- #
    def __init__(self, config):
        
        categories_file = config['COCO categories file']
        nlp_lib = config['NLP library']
        
        if 'similarity function' in config:
            similarity_fn = config['similarity function']
        else:
            similarity_fn = 'path'
            
        if 'all synsets' in config:
            self.all_synsets = config['all synsets']
        else:
            self.all_synsets = False
        
        # -------------------------------------------------------------------- #
        # objects processing initialization
        # -------------------------------------------------------------------- #
        # in this case, the MS-COCO name:categories will be loaded to be used as
        # reference to translate terms into
        # -------------------------------------------------------------------- #
        import json
        
        with open(categories_file, 'r') as file:
            names = {c['name']: c['supercategory'] for c in json.load(file)}
        
        self.names = names
        
        # -------------------------------------------------------------------- #
        # text processing initialization
        # -------------------------------------------------------------------- #
        self.nlp_lib = nlp_lib
        self.nouns_to_terms_fn = {
            'nltk': self.nltk_nouns_to_terms,
            'spacy': self.spacy_nouns_to_terms
        }
        
        if 'nltk' == nlp_lib:
            
            from nltk import download
            from nltk.corpus import wordnet
            
            download('wordnet')

            self.terms = wordnet
            if similarity_fn == "path":
                self.simi_fn = self.terms.path_similarity
                
            self.coco = {n: self._get_synsets(n) for n in self.names.keys()}

            self.supercategories = {n: self._get_synsets(n)
                                    for n in self.names.values()}

            self.sc2names = {sc: [n for n, s in self.names.items() if s == sc]
                             for sc in self.supercategories}
            
        elif 'spacy' == nlp_lib:
            from spacy import load
            self.spacy = load('en_vectors_web_lg')
            
            self.coco = [self.spacy(name) for name in self.names.keys()]
            
            self.supercategories = [self.spacy(sc) 
                                    for sc in self.names.values()]
            
        else:
            raise ImportError("NLP library not found")
        # -------------------------------------------------------------------- #
        
        
# ---------------------------------------------------------------------------- #
    def objects_to_terms(self, objects_info):
        objs = list(objects_info.keys())
        return {obj: (obj, self.names[obj]) for obj in objs}
        
# ---------------------------------------------------------------------------- #
    def nouns_to_terms(self, nouns):
        return self.nouns_to_terms_fn[self.nlp_lib](nouns)
        
# ---------------------------------------------------------------------------- #
    def spacy_nouns_to_terms(self, nouns):
        names_dict = {noun: {name.text: name.similarity(self.spacy(noun))
                             for name in self.coco}
                      for noun in nouns}
        
        names_dict = {noun: max(sims.items(), key=self.itemgetter(1))[0] 
                for noun, sims in names_dict.items()}
        
        sc_dict = {noun: {sc.text: sc.similarity(self.spacy(noun))
                          for sc in self.supercategories}
                   for noun in nouns}
        
        sc_dict = {noun: max(sims.items(), key=self.itemgetter(1))[0] 
                for noun, sims in sc_dict.items()}
        
        return {noun: (names_dict[noun], sc_dict[noun]) for noun in nouns}
        
# ---------------------------------------------------------------------------- #
    def nltk_nouns_to_terms(self, nouns):
        return {noun: self._get_most_similar(noun) for noun in nouns}
        
# ---------------------------------------------------------------------------- #
    def _get_synsets(self, word, pos='n'):
        
        synsets = self.terms.synsets(word, pos='n')
        
        if self.all_synsets:
            word_synset = self.product([word], synsets)

            def filter_fn(w_s): 
                return w_s[0] == w_s[1].name().split('.')[0]

            synsets = [w_s[1] for w_s in filter(filter_fn, word_synset)]
            
        else:
            synsets = [synsets[0]] if synsets else []
            
        return synsets
    
# --------------------------------------------------------------------------- #
    def _get_most_similar(self, word):
        
        name = "other"
        supercategory = "other"
        
        word_synsets = self._get_synsets(word)
        if word_synsets:
            sim = {k: max([self.simi_fn(*i)
                           for i in self.product(v, word_synsets)])
                   for k, v in self.coco.items() if v}

            name = max(sim.items(), key=self.itemgetter(1))[0]
            supercategory = self.names[name]
        
        return (name, supercategory)
    
# --------------------------------------------------------------------------- #
    def objects_to_planes(self, objs_info, sod_img):
        threshold = self.np.mean(sod_img)  # set threshold
        img = self.np.where(sod_img > threshold, 1, 0)  # binarize image
        
        # select only one object from each category
        objs_info = self._get_main_objects(objs_info)

        planes = dict()
        for name, objs in objs_info.items():
            planes[name] = list()
            for obj in objs:
                y0, x0, y1, x1 = obj['box']
                region = img[y0:y1, x0:x1]
                plane = self.stats.mode(region, axis=None)[0]
                planes[name].append('front' if plane == 1 else 'back')

        return planes, objs_info
    
# --------------------------------------------------------------------------- #
    def _get_main_objects(self, objs_info):

        return {name: self._get_main_object(objs) if len(objs) > 1 else objs
                for name, objs in objs_info.items()}
        
# --------------------------------------------------------------------------- #
    def _get_main_object(self, objs):
        
        l = list()  # should return a list just as the other inputs
        ref = 0
        main_obj = None

        for obj in objs:
            y0, x0, y1, x1 = obj['box']
            box_size = (y1 - y0) * (x1 - x0)

            if box_size > ref:
                ref = box_size
                main_obj = obj
                
        l.append(main_obj)
        
        return l
    
# --------------------------------------------------------------------------- #
