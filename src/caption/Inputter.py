class Inputter:
  
    import tensorflow as tf
    import numpy as np
    from PIL import Image
    
# ---------------------------------------------------------------------------- #
    def __init__(self, config):
        
        inference_graph = config['inference graph']
        label_map = config['label map']
        confidence = config['confidence']
        sod_path = config['SOD path']
        nlp_lib = config['NLP library']
        
        # -------------------------------------------------------------------- #
        # object detection initialization
        # -------------------------------------------------------------------- #
        from dodo_detector.detection import SingleShotDetector
        
        self.detector = SingleShotDetector(inference_graph, label_map,
                                           confidence=confidence)
        # -------------------------------------------------------------------- #
        
        
        # -------------------------------------------------------------------- #
        # Salient Object Detection (SOD)
        # -------------------------------------------------------------------- #
        self.sod_path = sod_path
        # -------------------------------------------------------------------- #
        
        # -------------------------------------------------------------------- #
        # text processing initialization
        # -------------------------------------------------------------------- #
        self.nlp_lib = nlp_lib
        self.nlp_noun_filter = {
            'nltk': self.nltk_noun_filter,
            'spacy': self.spacy_noun_filter
        }
        
        if 'nltk' == nlp_lib:
            from nltk import download, word_tokenize, pos_tag
            self.tokenizer = word_tokenize
            self.tagger = pos_tag
            # find nouns in sentences
            download('punkt')
            download('averaged_perceptron_tagger')
            
        elif 'spacy' == nlp_lib:
            from spacy import load
            self.tagger = load('en_core_web_sm')
            
        else:
            raise ImportError("NLP library not found")
        
        # -------------------------------------------------------------------- #
        
        
        # -------------------------------------------------------------------- #
        # Spatial relation prepositions initialization
        # -------------------------------------------------------------------- #
        self.prepositions = set([
            # A
            "above", "across", "against", "ahead of", "along", "among",  "at",
            "around", "atop", "alongside",
            # B
            "behind", "below", "beneath", "beside", "between", "by",
            # C
            # D
            # E
            # F
            "from",
            # G
            # H
            # I
            "in front of", "inside", "in", "into",
            # J
            # K
            # L
            "left of",
            # M
            # N
            "near", "nearby", "next to",
            # O
            "off", "out of", "on top of", "on", "over", "onto", "of",
            # P
            # Q
            # R
            "right of",
            # S
            # T
            "through", "toward", "thru", "trough",
            # U
            "under", "up",
            # V
            # W
            "within",
            # X
            # Y
            # Z
        ])
        # -------------------------------------------------------------------- #
        
        
# ---------------------------------------------------------------------------- #
    def object_detection(self, image):
        marked_img, objs_info = self.detector.from_image(image)
        return objs_info, marked_img
    
# ---------------------------------------------------------------------------- #
    def sod_mapping(self, image):
    
        # tensorflow configuration
        gpu_fraction = 1
        gpu_options = self.tf.compat.v1.GPUOptions(
            per_process_gpu_memory_fraction=gpu_fraction)
        g_mean = self.np.array(([126.88, 120.24, 112.19])).reshape([1, 1, 3])

        # describe the actual work
        with self.tf.Session(
                config=self.tf.compat.v1.ConfigProto(
                    gpu_options=gpu_options)) as sess:

            saver = self.tf.compat.v1.train.import_meta_graph(
                self.sod_path + '/meta_graph/my-model.meta')

            saver.restore(sess, self.tf.compat.v1.train.latest_checkpoint(
                          self.sod_path + '/salience_model'))

            image_batch = self.tf.compat.v1.get_collection('image_batch')[0]
            pred_mattes = self.tf.compat.v1.get_collection('mask')[0]

            img = self.np.array(image.resize([320, 320], self.Image.ANTIALIAS))
            img = self.np.expand_dims(img - g_mean, 0)

            # finally, do something
            feed_dict = {image_batch: img}
            pred_alpha = sess.run(pred_mattes, feed_dict=feed_dict)
            final_alpha = self.np.squeeze(pred_alpha)

            final_img = self.np.array(
                self.Image.fromarray(final_alpha).resize(image.size))
          
        return final_img
        
# ---------------------------------------------------------------------------- #
    def noun_filter(self, caption):
        return self.nlp_noun_filter[self.nlp_lib](caption)
        
# ---------------------------------------------------------------------------- #
    def spacy_noun_filter(self, caption):
        return [token.text for token in self.tagger(caption)
                if token.pos_ is 'NOUN']
    
# ---------------------------------------------------------------------------- #
    def nltk_noun_filter(self, caption):
        word_tag = self.tagger(self.tokenizer(caption))
        return [word for word, wtag in word_tag if wtag[0] == 'N']
    
# ---------------------------------------------------------------------------- #
    def spatial_relation_filter(self, caption):
        word_tag = self.tagger(self.tokenizer(caption))
        tags = [word for word, wtag in word_tag if wtag == 'IN']
        return [word for word in tags if word in self.prepositions]
    
# ---------------------------------------------------------------------------- #
