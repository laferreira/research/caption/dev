class Comparator:
    
    import itertools as it
    
# ---------------------------------------------------------------------------- #
    def compare_nouns(self, caption, image):
        nouns = caption.copy()
        objs = image.copy()

        capt = dict()
        img = dict()
        both = dict()
        found_in_img = set()

        while nouns:
            noun, term_n = nouns.popitem()
            found = False
            for obj, term_o in objs.items():
                if term_n == term_o:
                    both[(noun, obj)] = term_n
                    found = True
                    found_in_img.add(obj)
            if not found:
                capt[noun] = term_n

        for k in set(objs.keys()) - found_in_img:
            img[k] = objs[k]
            
        return both, capt, img
      
# ---------------------------------------------------------------------------- #
    def get_objects_spatial_relation(self, ref, obj):
        ref_v = (ref[0], ref[2])
        ref_h = (ref[1], ref[3])
        
        obj_v = (obj[0], obj[2])
        obj_h = (obj[1], obj[3])
        
        rel_h = self._horizontal_relation(ref_h, obj_h)
        rel_v = self._vertical_relation(ref_v, obj_v)
        
        return (rel_h, rel_v)
    
# ---------------------------------------------------------------------------- #
# Spatial relation support functions
# ---------------------------------------------------------------------------- #
    def _point_relation(self, ref, p):
        """
        B: before 
          p * 
              o----------o
            start       end
              
        S: start 
              * p
              o----------o
            start       end
              
        I: inside 
                  * p
              o----------o
            start       end
              
        E: end 
                         * p
              o----------o
            start       end
              
        A: after 
                           * p
              o----------o
            start       end
        """
        
        ref_start, ref_end = ref
        
        if p < ref_start:
            return "B"
        elif p == ref_start:
            return "S"
        elif p > ref_start and p < ref_end:
            return "I"
        elif p == ref_end:
            return "E"
        elif p > ref_end:
            return "A"
        
        return None

# ---------------------------------------------------------------------------- #
    def _line_relation(self, ref, line):
        """
        -: impossible relation
        .: line is a point (starts and ends at the same value)
        
        B: before
        A: after
        I: inside
        E: equal
        S: surrounding
        
        OB: overlaps before
        OA: overlaps after
        
        TOB: touches outside before
        TOA: touches outside after
        
        TIB: touches inside before  
        TIA: touches inside after
        
        BTA: starts before, touches after
        TBA: touches before, ends after
        
        B -> TOB -> OB -> TIB -> I -> TIA -> OA -> TOA -> A
        
          | B | S | I | E | A 
        --|---|---|---|---|---
        B | B |TOB|OB |BTA| S
        S | - | . |TIB| E |TBA
        I | - | - | I |TIA|OA
        E | - | - | - | . |TOA
        A | - | - | - | - | A
        """
        
        # just as the table above
        relations = {
            "B": {"B": "B", "S": "TOB", "I": "OB",  "E": "BTA", "A": "S"},
            "S": {"I": "TIB", "E": "E", "A": "TBA"},
            "I": {"I": "I", "E": "TIA", "A": "OA"},
            "E": {"A": "TOA"},
            "A": {"A": "A"}
        }

        line_start, line_end = line
        start = self._point_relation(ref, line_start)
        end = self._point_relation(ref, line_end)
        
        if end in relations[start]:
            return relations[start][end]
        
        return None
    
# ---------------------------------------------------------------------------- #
    def _get_description(self, ref, line, substitutions):

        rel = self._line_relation(ref, line)
        
        # substitute letters following a dict
        rel = [l if l not in substitutions else substitutions[l] for l in rel]
        rel = "".join(rel)  # and join the letters to form a string
        
        return rel
    
# ---------------------------------------------------------------------------- #
    def _horizontal_relation(self, ref, line):
        
        substitutions = {"B": "L", "A": "R"}
        return self._get_description(ref, line, substitutions)
    
# ---------------------------------------------------------------------------- #
    def _vertical_relation(self, ref, line):
        
        substitutions = {"B": "A", "A": "B"}
        return self._get_description(ref, line, substitutions)

# ---------------------------------------------------------------------------- #
    def compare_planes(self, objs_planes, nouns):
        both, image = nouns.values()
        p_swap = dict()

        for o1, o2 in self.it.permutations(both, 2):
            if objs_planes[o1[-1]] == objs_planes[o2[-1]]:
                p_swap[(o1, o2)] = []
            else:
                p_swap[(o1, o2)] = [(o1, o) for o in image
                                    if objs_planes[o] == objs_planes[o1[-1]]
                                    and image[o][-1] == both[o2][-1]]
        return p_swap
    
# ---------------------------------------------------------------------------- #
