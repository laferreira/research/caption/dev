class Caption:
    
# ---------------------------------------------------------------------------- #
    from numpy import array
    
# ---------------------------------------------------------------------------- #
    def __init__(self, config):
        
        from .Inputter import Inputter
        from .Translator import Translator
        from .Comparator import Comparator
        from .TaskSolver import TaskSolver
        
        self.check_noun = ('noun' in config['error types'] or 
                          'Noun' in config['error types'] or
                          'NOUN' in config['error types'])
            
        self.check_planes = ('planes' in config['error types'] or 
                            'Planes' in config['error types'] or 
                            'PLANES' in config['error types'])
        
        self.inputter = Inputter(config['inputter'])        # L1
        self.translator = Translator(config['translator'])  # L2
        self.comparator = Comparator()                      # L3
        self.solver = TaskSolver()                          # L4
        
        self.__list_available_hw__()
        
# ---------------------------------------------------------------------------- #
    def __list_available_hw__(self):
        import tensorflow as tf
        
        n_gpus = len(tf.config.experimental.list_physical_devices('GPU'))
        n_cpus = len(tf.config.experimental.list_physical_devices('CPU'))
        
        output = f'- GPUs available: {n_gpus if n_gpus else 0}\n'
        output += f'- CPUs available: {n_cpus if n_cpus else 0}\n'
        print(output)
        
# ---------------------------------------------------------------------------- #
    def _find_nouns_foils(self, caption, image):
        corrections = self.solver.task3_noun(caption, image)
        foil_words = self.solver.task2_solver(corrections)
        foil_check = self.solver.task1_solver(foil_words)
        
        return foil_check, foil_words, corrections
    
# ---------------------------------------------------------------------------- #
    def _find_planes_foils(self, planes_comp):
        corrections = self.solver.task3_plane(planes_comp)
        foil_words = self.solver.task2_solver(corrections)
        foil_check = self.solver.task1_solver(foil_words)
        
        return foil_check, foil_words, corrections
    
# ---------------------------------------------------------------------------- #
    def find_foil(self, image, caption):
        
        # --------------------------------------------------------------------- #
        # Create lists to add tasks solutions and dict to return the results
        # --------------------------------------------------------------------- #
        error_types = list()
        foil_list = list()
        correction_list = list()
        
        results = dict()
        results['debug'] = dict()
        # --------------------------------------------------------------------- #
    
        # --------------------------------------------------------------------- #
        # Inputter calls
        # --------------------------------------------------------------------- #
        img_array = self.array(image)
        objs_info, marked_img = self.inputter.object_detection(img_array)
        sod_img = self.inputter.sod_mapping(image)
        nouns = self.inputter.noun_filter(caption)
        
        results['debug']['images'] = (image, marked_img, sod_img)
        results['debug']['objs_info'] = objs_info
        # --------------------------------------------------------------------- #

        # --------------------------------------------------------------------- #
        # Translator calls
        # --------------------------------------------------------------------- #
        objs_terms = self.translator.objects_to_terms(objs_info)
        nouns_terms = self.translator.nouns_to_terms(nouns)
        # --------------------------------------------------------------------- #

        
        # --------------------------------------------------------------------- #
        # Check for foils in nouns
        # --------------------------------------------------------------------- #
        if self.check_noun:
            
            terms = self.comparator.compare_nouns(nouns_terms, objs_terms)
            both_t, caption_t, image_t = terms
            
            noun_tasks = self._find_nouns_foils(caption_t, image_t)
            nount_t1, nount_t2, nount_t3 = noun_tasks
            
            results['debug']['terms'] = nouns_terms
                
            if nount_t1:
                error_types.append('Noun')
                foil_list.append(nount_t2)
                correction_list.append(nount_t3)
        # --------------------------------------------------------------------- #
                
        # --------------------------------------------------------------------- #
        # Check for foils using planes comparison
        # --------------------------------------------------------------------- #
        if self.check_planes:
            
            if not ('both_t' in locals() and 'caption_t' in locals() and 
                    'image_t' in locals()):
                
                terms = self.comparator.compare_nouns(nouns_terms, objs_terms)
                both_t, caption_t, image_t = terms
            
            objs_planes, planes_info = self.translator.objects_to_planes(
                                                       objs_info, sod_img)
        
            to_compare = {
                "both_t": both_t,
                "image_t": image_t
            }
            
            planes_comp = self.comparator.compare_planes(objs_planes, to_compare)
            
            planes_tasks = self._find_planes_foils(planes_comp)
            plane_t1, plane_t2, plane_t3 = planes_tasks
            
            results['debug']['planes_info'] = planes_info
                
            if plane_t1:
                error_types.append('Planes')
                foil_list.append(plane_t2)
                correction_list.append(plane_t3)
        # --------------------------------------------------------------------- #

        results['tasks'] = self.solver.format_output(error_types, foil_list,
                                                    correction_list)

        return results
# ---------------------------------------------------------------------------- #
