class DataHandler:

    from matplotlib import pyplot as plt
    from matplotlib import patches
    from PIL import Image

# ---------------------------------------------------------------------------- #
    def __init__(self, config, init_table=True):
        import sqlite3

        foil_path = config['foil path']
        images_path = config['images path']
        table_name = config['table name']
        db_name = config['db name']

        # -------------------------------------------------------------------- #
        # FOIL dataset initialization
        # -------------------------------------------------------------------- #
        import json

        with open(foil_path) as file:
            self.foil_data = json.load(file)['annotations']
        # -------------------------------------------------------------------- #

        # -------------------------------------------------------------------- #
        # MS-COCO images path
        # -------------------------------------------------------------------- #
        self.images_path = images_path
        # -------------------------------------------------------------------- #


        # -------------------------------------------------------------------- #
        # Use SQLite to save the results
        # -------------------------------------------------------------------- #
        self.db_name = db_name
        self.table_name = table_name

        self.conn = sqlite3.connect(db_name)
        self.cursor = self.conn.cursor()

        if init_table:
            try:
                self.cursor.execute(f'''CREATE TABLE {table_name} (id INT,
                    foilid INT, foil BOOL, foilword TEXT, foiltarget TEXT,
                    t1 BOOL, t2 TEXT, t3 TEXT, r1 BOOL, r2 BOOL, r3 BOOL,
                    UNIQUE(id, foilid));''')
                print("- Table created")
                self.conn.commit()
            except sqlite3.OperationalError:
                print("- Table already exists")
        # -------------------------------------------------------------------- #

# ---------------------------------------------------------------------------- #
    def get_data(self, foil_entry):

        try:
            image_file = (str(foil_entry['image_id']) + '.jpg').rjust(16, '0')
            image = self.Image.open(self.images_path + '/' + image_file)

            caption = foil_entry['caption']

            return image, caption, foil_entry

        except FileNotFoundError:
            raise(FileNotFoundError(f"Entry {foil_entry} no found"))
            
# ---------------------------------------------------------------------------- #
    def get_last_processed_image(self):

        last = self.cursor.execute(f"select max(id) from {self.table_name}")
        last = last.fetchone()[0]

        return last if last else -1

# ---------------------------------------------------------------------------- #
    def generate_comparison_line(self, caption, foil):
        from itertools import chain

        t1, t2, t3 = caption

        t2 = set(chain(*t2.values()))
        t2 = 'ORIG' if not t2 else ' '.join(t2)

        t3 = set(chain(*[i for x in t3.values() for i in x.values()]))
        t3 = 'ORIG' if not t3 else ' '.join(t3)

        foil_id = foil['foil_id']
        is_foil = foil['foil']
        foil_word = foil['foil_word']
        correct_word = foil['target_word']

        return foil_id, is_foil, foil_word, correct_word, t1, t2, t3

# ---------------------------------------------------------------------------- #
    def get_results(self, line):
        _, foil, foil_word, foil_correction, t1, t2, t3 = line

        solved_t1 = t1 == foil
        solved_t2 = foil_word in t2 if solved_t1 else False
        solved_t3 = foil_correction in t3 if solved_t2 else False

        return solved_t1, solved_t2, solved_t3

# ---------------------------------------------------------------------------- #
    def save_results(self, n_img, raw_results, processed_results):
        self.cursor.execute(f'''INSERT INTO {self.table_name} VALUES
                            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                            (tuple([n_img]) + raw_results + processed_results))
        self.conn.commit()

# ---------------------------------------------------------------------------- #
    def close_all(self):
        self.conn.close()

# ---------------------------------------------------------------------------- #
    def print_results(self):
        from numpy import array

        results = self.cursor.execute(f'''SELECT r1, r2, r3
                                          from {self.table_name}''')
        results = array(list(results))

        t1, t2, t3 = array(results).sum(axis=0)
        total = len(results)

        print("Final results:")
        print(f"Task 1: {t1} of {total} ({100 * t1 /  total:.2f}%)")
        print(f"Task 2: {t2} of {total} ({100 * t2 / total:.2f}%)")
        print(f"Task 3: {t3} of {total} ({100 * t3 / total:.2f}%)")

# ---------------------------------------------------------------------------- #
