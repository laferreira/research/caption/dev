class Tester:
    
# ----------------------------------------------------------------------------- #
    def __init__(self, config, init_table=True):
        
        self.print_line()
        print("Initializing objects...")

        from DataHandler import DataHandler
        self.handler = DataHandler(config['data handler'], init_table)
        print("Data handler initialized")

        from caption import Caption
        self.caption = Caption(config)
        print("CAPTION initialized")
        
        self.print_line()
        
# ----------------------------------------------------------------------------- #
    def run_test(self, foil_entry):
        
        image, caption, entry = self.handler.get_data(foil_entry)
        results = self.caption.find_foil(image, caption)
        raw_res = self.handler.generate_comparison_line(results['tasks'],
                                                   foil_entry)
        res = self.handler.get_results(raw_res)
            
        return raw_res, res, results

# ----------------------------------------------------------------------------- #
    def print_line(self, char='-', size=80):
        print(char*size)
    
# ---------------------------------------------------------------------------- #
# If called from the command line...
# ---------------------------------------------------------------------------- #

if __name__ == '__main__':
    
    import sys
    
    from config import default_config as caption_config
    
    tester = Tester(caption_config)

    n_imgs = int(sys.argv[1])
    ini_img = tester.handler.get_last_processed_image()

    from_pos = ini_img + 1
    to_pos = from_pos + n_imgs

    not_processed = 0

    print(f"Starting CAPTION loop from image {from_pos} up to {to_pos - 1}:")
    for n, entry in enumerate(tester.handler.foil_data[from_pos:to_pos], 
                              start=from_pos):

        print(f"Image: {n - ini_img} of {to_pos - ini_img - 1}")
        
        try:
            raw_res, res, results = tester.run_test(entry)
            tester.handler.save_results(n, raw_res, res)
            
        except FileNotFoundError as fnfe:
            not_processed += 1
            
        except ValueError:
            not_processed += 1

    tester.print_line()
    print(f"{not_processed} images weren't processed")
    tester.print_line()
    tester.handler.print_results()
    tester.print_line()
    
    tester.handler.close_all()
