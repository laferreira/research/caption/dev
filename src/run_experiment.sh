#!/bin/bash

for i in `seq $1`
do
    ldconfig
    python3 main.py $2 2> /dev/null
done
