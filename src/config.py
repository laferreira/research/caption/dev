# ---------------------------------------------------------------------------- #
#                                  Paths                                       #
# ---------------------------------------------------------------------------  #

# ---------------------------------------------------------------------------  #
# Tensorflow modelzoo
# ---------------------------------------------------------------------------- #

model = 'faster_rcnn_nas_coco_2018_01_28'
label_map = 'mscoco_label_map.pbtxt'

tf_path = 'data/tensorflow/'
inference_graph_path = tf_path + model + '/frozen_inference_graph.pb'
label_map_path = tf_path + label_map

# ---------------------------------------------------------------------------- #
# SOD
# ---------------------------------------------------------------------------- #
sod_path = 'data/sod'

# ---------------------------------------------------------------------------- #
# Foil dataset
# ---------------------------------------------------------------------------- #

foil_ds_path = "data/foil/"
foil_train = foil_ds_path + "foilv1.0_train_2017.json"
foil_test = foil_ds_path + "foilv1.0_test_2017.json"

# ---------------------------------------------------------------------------- #
# MS-COCO images
# ---------------------------------------------------------------------------- #

coco_path = "data/mscoco/"
coco_train = coco_path + "train2017"
coco_test = coco_path + "test2017"
coco_val = coco_path + "val2017"
coco_categories = coco_path + 'categories.json'

# ---------------------------------------------------------------------------- #
# Databases
# ---------------------------------------------------------------------------  #

results_database_path = "caption.db"

# ---------------------------------------------------------------------------- #
#                        CAPTION Configuration                                 #
# ---------------------------------------------------------------------------  #
default_config = dict()

default_config['error types'] = ['noun', 'planes']

default_config['data handler']= {
    'foil path': foil_test,
    'images path': coco_train,
    'table name': 'spacy',
    'db name': results_database_path,
}

default_config['inputter'] = {
        'inference graph': inference_graph_path,
        'label map': label_map_path,
        'confidence': 0.5,
        'SOD path': sod_path,
        'NLP library': 'spacy'  # 'nltk'
}
    
default_config['translator'] = {
        'COCO categories file': coco_categories,
        'NLP library': 'spacy',  # 'nltk'
        'similarity function': 'path',
        'all synsets': False  # True
}